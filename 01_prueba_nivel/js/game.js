window.logs = [];

function vuelta(id) {
    let card = document.getElementById("carta-" + id);
    if (card.classList.contains("anverso")) {
        card.classList.remove("anverso");
        log(card.id + " Mostrada");
    } else {
        card.classList.add("anverso")
        log(card.id + " Ocultada");
    }
    checkCards();
}

function checkCards() {
    let deck = [];
    let board = document.getElementById("board");
    let visibleCards = [];
    let hiddenCards = [];

    deck = [...board.children];

    deck.forEach(card => {
        if (card.classList.contains("anverso")) {
            hiddenCards.push(card);
        } else {
            visibleCards.push(card);
        }
    });

    if (isMatch("diamantes", visibleCards) || isMatch("picas", visibleCards)) {

        log("Juego ganado");

        let youWon = document.getElementById("ganas");
        youWon.classList.remove("hidden");

        let playAgain = document.getElementById("repetir");
        playAgain.classList.remove("hidden");
    } else if (visibleCards.length == 2) {

        log("No coinciden las dos cartas sel");

        hideAll(deck);
    }
}

function isMatch(type, cards) {
    cards = cards.filter(card => {
        return card.classList.contains(type);
    });
    console.log(type, cards);
    if (cards.length > 1) {
        return true;
    }
    return false;
}

function hideAll(deck) {

    let youWon = document.getElementById("ganas");
    youWon.classList.add("hidden");

    let playAgain = document.getElementById("repetir");
    playAgain.classList.add("hidden");

    setTimeout(function () {
        deck.forEach(card => {
            if (!card.classList.contains("anverso")) {
                card.classList.add("anverso");
            }
        });
    }, 700);

}

function nuevoJuego() {
    hideAll([...document.getElementById('board').children]);
    log("Nuevo juego");
}

function log(message) {

    window.logs.push(message);
    console.log(window.logs);
    sessionStorage.setItem('historial', window.logs);

}