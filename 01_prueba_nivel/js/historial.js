window.onload = function () {
    console.log("Historial");
    let logs = getLogs();
    if (logs != null) {
        showLogs(logs);
    }
}

function getLogs() {
    let logs = sessionStorage.getItem('historial');
    console.log(logs.split(','));
    if (logs == null) {
        return [];
    }
    return logs.split(',');
}

function showLogs(logs) {
    let logsContainer = document.getElementById('logsContainer');
    logs.forEach(log => {
        console.log("->", log);
        let tr = document.createElement("tr")
        let td = document.createElement("td");
        let text = document.createTextNode(log);

        tr.appendChild(td);
        td.appendChild(text);
        logsContainer.appendChild(tr);

    });
}